import React, { useState, useEffect } from 'react';
import { Purchase, Gameover, Error, Loading } from '.';
import { Game, Memo, Items as ItemsType, Receipt, browse, purchase, longings } from '../api';
import Item from '../components/item';
import Items from '../components/items';
import Modal from '../components/modal';

export type GameShop = {
    game: Game;
    update: Function;
    quit: Function;
    return: Function;
}

export function Shop (props: GameShop) {
    const [memo, setMemo] = useState<Memo>({});
    const [items, setItems] = useState<ItemsType>([]);
    const [receipt, setReceipt] = useState<Receipt | null>(null);
    const [diff, setDiff] = useState<Receipt | null>(null);
    const [loading, setLoading] = useState<Boolean>(false);
    const [longing, setLonging] = useState(longings.items);

    const buy = (itemId: string) => {
        setLoading(true);
        setLonging(longings.receipt);

        purchase(props.game.gameId, itemId).then(check => {
            setLoading(false);

            if ('error' in check || 'status' in check) {
                setMemo(check as Memo);

                return;
            }

            const c = check as Receipt;

            setDiff({
                gold: c.gold - props.game.gold,
                lives: c.lives - props.game.lives,
                level: c.level - props.game.level,
                turn: c.turn - props.game.turn
            });

            setReceipt({ ...c, itemId });

            props.update({
                gold: c.gold,
                lives: c.lives,
                level: c.level,
                turn: c.turn
            });
        });
    };
    const close = (gameover: Boolean = false) => {
        setReceipt(null);
        setMemo(gameover ? { status: 'Game Over' } : {});
    };
    const back = () => {
        props.return();
    }
    const cancel = () => {
        setLoading(false);
    };

    useEffect(() => {
        setLoading(true);

        browse(props.game.gameId).then(observsation => {
            setLoading(false);

            if ('status' in observsation || 'error' in observsation) {
                setMemo(observsation as Memo);
            } else {
                setItems(observsation as ItemsType);
            }
        });
    }, []);

    return <>
        {loading ? <Loading modal={items.length > 0} message={longing} cancel={items.length > 0 ? cancel : null} /> : ''}

        {memo.status === 'Game Over' ? (<Modal open={memo.status === 'Game Over'}><Gameover return={props.quit}/></Modal>) : ''}
        {memo.error ? (<Modal open={memo.error ? true : false}><Error heading={memo.error} return={close} /></Modal>) : ''}

        {receipt ? (<Modal open={receipt ? true : false}><Purchase data={receipt} diff={diff as Receipt} return={close} /></Modal>) : ''}

        {items.length > 0 ? <Items>{items.map((item, index) => <Item key={index} data={item} buy={buy} gold={props.game.gold} />)}</Items> : loading ? '' : <><h2>Shop is closed</h2><p>No items are available for purchase...</p></>}

        <footer className="buttonbar">
            <button onClick={back} className="-return">Return</button>
        </footer>
    </>;
}
