import React from 'react';
import { Solution, changes, match, icons } from '../api';

export type GameSolution = {
    data: Solution;
    diff: Solution;
    return: Function;
}

export function Solution (props: GameSolution) {
    const click = () => {
        props.return(props.data.lives === 0);
    };

    return <section className="solution">
        <h2>{props.data.message}</h2>

        <div className="solution-icon -oversize" onClick={click}>
            {match(props.data.success ? 'success' : 'defeat', icons.status)}
        </div>

        <ul className="report">
            {changes(props.diff).map(status => <li key={status.key}>
                {status.icon}
                <strong>{status.value}</strong>
            </li>)}
        </ul>

        <div className="buttonbar -single">
            <button onClick={click}>OK</button>
        </div>
    </section>
}
