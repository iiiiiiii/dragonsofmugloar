import React from 'react';
import { Memo } from '../api';
import { Error, Loading } from '.';
import Modal from '../components/modal';

export type GameNew = {
    start: Function;
    memo: Memo;
    setMemo: Function;
    loading: Boolean;
}

export function New (props: GameNew) {
    const start = () => {
        props.start()
    };
    const close = (gameover: Boolean = false) => {
        props.setMemo(gameover ? { status: 'Game Over' } : {});
    };

    return <>
        {
            props.loading ? 
                <Loading />
            :
                <div className="buttonbar -single -special">
                    <button onClick={start}>Start</button>
                </div>
        }

        {
            props.memo.error ? 
                <Modal open={props.memo.error ? true : false}>
                    <Error heading={props.memo.error} return={close} />
                </Modal>
            : ''
        }

        {
            props.memo.status ? 
                <Modal open={props.memo.status ? true : false}>
                    <Error heading={props.memo.status} return={close} />
                </Modal>
            : ''
        }
    </>;
}
