import React from 'react';

import { Receipt, changes, match, icons } from '../api';

export type GamePurchase = {
    data: Receipt;
    diff: Receipt;
    return: Function;
}

export function Purchase (props: GamePurchase) {
    const click = () => {
        props.return();
    };

    return <section className="purchase">
        <h2>{props.data.shoppingSuccess ? 'Item aquired' : 'Not enough gold'}</h2>

        <div className="purchase-icon -oversize" onClick={click}>
            {props.data.shoppingSuccess ? props.data.itemId ? match(props.data.itemId, icons.item) : match('success', icons.status) : match('defeat', icons.status)}
        </div>

        <ul className="report">
            {changes(props.diff).map(status => <li key={status.key}>{status.icon} {status.value}</li>)}
        </ul>

        <div className="buttonbar -single">
            <button onClick={click}>OK</button>
        </div>
    </section>
}
