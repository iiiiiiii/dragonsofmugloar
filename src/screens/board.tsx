import React, { useState, useEffect } from 'react';
import { Solution, Gameover, Error, Loading } from '.';
import { Game, read, solve, Messages as MessagesType, Memo, Solution as SolutionType, longings, encrypt } from '../api';
import Message from '../components/message';
import Messages from '../components/messages';
import Modal from '../components/modal';

export type GameBoard = {
    game: Game;
    update: Function;
    quit: Function;
    return: Function;
}

export function Board (props: GameBoard) {
    const [memo, setMemo] = useState<Memo>({});
    const [messages, setMessages] = useState<MessagesType>([]);
    const [solution, setSolution] = useState<SolutionType | null>(null);
    const [diff, setDiff] = useState<SolutionType | null>(null);
    const [loading, setLoading] = useState<Boolean>(false);
    const [longing, setLonging] = useState(longings.quests);

    const attempt = (adId: string) => {
        setLoading(true);
        setLonging(longings.solution);

        solve(props.game.gameId, adId).then(solution => {
            setLoading(false);

            setMessages(messages.map(message => message.adId === encrypt(adId, message.encrypted) ? { ...message, expired: true } : message));

            if ('error' in solution || 'status' in solution) {
                setMemo(solution as Memo);

                return;
            }

            const sol = solution as SolutionType;
            
            setDiff({
                lives: sol.lives - props.game.lives,
                gold: sol.gold - props.game.gold,
                score: sol.score - props.game.score,
                highScore: sol.highScore - props.game.highScore,
                turn: sol.turn - props.game.turn
            });

            setSolution(sol);

            props.update({
                lives: sol.lives,
                gold: sol.gold,
                score: sol.score,
                highScore: sol.highScore,
                turn: sol.turn
            });
        });
    };
    const close = (gameover: Boolean = false) => {
        setSolution(null);
        setMemo(gameover ? { status: 'Game Over' } : {});
    };
    const back = () => {
        props.return();
    }
    const cancel = () => {
        setLoading(false);
    };
    
    useEffect(() => {
        setLoading(true);

        read(props.game.gameId).then(reading => {
            setLoading(false);

            if ("status" in reading) {
                setMemo(reading as Memo);
            } else if ("error" in reading) {
                setMemo(reading as Memo);
            } else {
                setMessages(reading as MessagesType);
            }
        });
    }, []);

    return <>
        {loading ? <Loading modal={messages.length > 0} message={longing} cancel={messages.length > 0 ? cancel : null} /> : ''}

        {memo.status === 'Game Over' ? (<Modal open={memo.status === 'Game Over'}><Gameover return={props.quit}/></Modal>) : ''}
        {memo.error ? (<Modal open={memo.error ? true : false}><Error heading={memo.error} return={close} /></Modal>) : ''}

        {solution ? (<Modal open={solution ? true : false}><Solution data={solution as SolutionType} diff={diff as SolutionType} return={close} /></Modal>) : ''}

        {messages.length > 0 ? <Messages>{messages.map(message => <Message key={message.adId} game={props.game} data={message} solve={attempt} />)}</Messages> : loading ? '' : <><h2>Notice boards is empty</h2><p>No messages so far...</p></>}

        <footer className="buttonbar">
            <button onClick={back} className="-return">Return</button>
        </footer>
    </>;
}
