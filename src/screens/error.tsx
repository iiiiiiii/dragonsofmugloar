import React from 'react';
import { icons, match } from '../api';

export type GameError = {
    heading: string
    return: Function;
}

export function Error (props: GameError) {
    const click = () => {
        props.return();
    };

    return <section className="error">
        <h2 className="error-heading">{props.heading}</h2>

        <p className="error-icon -oversize">{match('error', icons.status)}</p>

        <div className="buttonbar -single">
            <button onClick={click}>OK</button>
        </div>
    </section>
}
