import React from 'react';
import { icons, match } from '../api';
import Modal from '../components/modal';

export type GameLoading = {
    message?: string;
    cancel?: Function | null;
    modal?: Boolean;
}

export function Loading (props: GameLoading) {
    const cancel = () => {
        if (props.cancel) {
            props.cancel();
        }
    };
    const content = <>
        <section className="loading">
            <h2>{props.message ? props.message : 'Engaging the Dragon...'}</h2>

            <div className="loading-icon -oversize">{match('loading', icons.status)}</div>
        </section>

        {
            props.modal ?
                <div className="buttonbar -single">
                    <button onClick={cancel} className="-return">Close</button>
                </div>
            : props.cancel ?
                <footer className="buttonbar">
                    <button onClick={cancel} className="-return">Cancel</button>
                </footer>
            : ''
        }
    </>;

    return props.modal ? <Modal open>{content}</Modal> : content;
}
