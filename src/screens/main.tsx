import React, { useState } from 'react';
import { Game, investigate, Reputation as ReputationType, Memo, longings } from '../api';
import { Gameover, Error, Loading } from '.';
import Modal from '../components/modal';
import { Reputation } from './reputation';

export type GameMain = {
    game: Game;
    navigate: Function;
    quit: Function;
}

export function Main (props: GameMain) {
    const [memo, setMemo] = useState<Memo>({});
    const [leaving, setLeaving] = useState<boolean>(false);
    const [reputation, setReputation] = useState<ReputationType | null>(null);
    const [loading, setLoading] = useState<Boolean>(false);

    const leave = () => {
        setLeaving(true);
    };
    const stay = () => {
        setLeaving(false);
    };
    const quit = () => {
        props.quit();
    }
    const listen = () => {
        setLoading(true);

        investigate(props.game.gameId).then(whisper => {
            setLoading(false);

            if ('error' in whisper || 'status' in whisper) {
                setMemo(whisper as Memo);

                return;
            }

            setReputation(whisper as ReputationType);
        });
    };
    const cancel = () => {
        setLoading(false);
    };
    const close = (gameover: Boolean = false) => {
        setReputation(null);
        setMemo(gameover ? { status: 'Game Over' } : {});
    };

    return <>
        {memo.status === 'Game Over' ? (<Modal open={memo.status === 'Game Over'}><Gameover return={props.quit}/></Modal>) : ''}
        {memo.error ? (<Modal open={memo.error ? true : false}><Error heading={memo.error} return={close} /></Modal>) : ''}

        {loading ? <Loading modal message={longings.reputation} cancel={cancel} /> : <>
            <h2>Once in the Mugloar...</h2>
            <div className="buttonbar -main">
                <button type="button" onClick={() => props.navigate('board')}>Read messages</button>
                <button type="button" onClick={() => props.navigate('shop')}>Visit shop</button>
                <button type="button" onClick={listen}>Hear whispers</button>
                <button type="button" onClick={leave}>Leave the Kingdom</button>
            </div>
        </>}

        {leaving ? <Modal open={leaving}>
            <h2>Abandon the quest?</h2>
            <div className="buttonbar -spread">
                <button type="button" onClick={quit}>Yes</button>
                <button type="button" onClick={stay}>No</button>
            </div>
        </Modal> : ''}

        {reputation ? <Modal open={reputation ? true : false}>
            <Reputation data={reputation} return={close} />
        </Modal> : ''}
    </>
}
