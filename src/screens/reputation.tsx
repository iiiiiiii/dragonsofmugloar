import React from 'react';

import { Reputation, match, icons } from '../api';

export type GameReputation = {
    data: Reputation;
    return: Function;
}

export function Reputation (props: GameReputation) {
    const click = () => {
        props.return();
    };
    const round = (number: number) => Math.round(number * 1000) / 1000;

    return <section className="reputation">
        <h2>Reputation</h2>

        <ul className="report">
            <li>
                {match('people', icons.reputation)}
                <strong>{round(props.data.people)}</strong>
            </li>
            <li>
                {match('state', icons.reputation)}
                <strong>{round(props.data.state)}</strong>
            </li>
            <li>
                {match('underworld', icons.reputation)}
                <strong>{round(props.data.underworld)}
            </strong></li>
        </ul>

        <div className="buttonbar -single">
            <button onClick={click}>OK</button>
        </div>
    </section>
}
