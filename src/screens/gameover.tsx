import React from 'react';
import { icons, match } from '../api';

export type GameOver = {
    return: Function;
}

export function Gameover (props: GameOver) {
    const click = () => {
        props.return();
    };

    return <section className="gameover">
        <h2 className="gameover-heading">Game Over</h2>

        <p className="gameover-icon -oversize">{match('dead', icons.status)}</p>

        <div className="buttonbar -single">
            <button onClick={click}>OK</button>
        </div>
    </section>
}
