const kingdom = 'https://dragonsofmugloar.com/api/v2/';
const headers = {
    'Content-Type': 'application/json',
};

export type Game = {
    gameId: String,
    lives: number,
    gold: number,
    level: number,
    score: number,
    highScore: number,
    turn: number
};

export async function start (): Promise<Game | Memo> {
    try {
        const response = await fetch(kingdom + 'game/start', {
        method: 'POST',
        headers
        });
    
        return response.json();
    } catch (e) {
        return {
            error: 'The Dragon is sleeping...'
        }
    }
}

export type Memo = {
    status?: string;
    error?: string
}

export type Reputation = {
    people: number;
    state: number;
    underworld: number;
};

export async function investigate (gameId: String): Promise<Reputation | Memo> {
    try {
        const response = await fetch(kingdom + gameId + '/investigate/reputation', {
            method: 'POST',
            headers
        });
    
        return response.json();
    } catch (e) {
        return {
            error: 'The village is silent, the kingdom is empty, the underworld is deserted...'
        }
    }
}

export type Message = {
    adId: string;
    message: string;
    reward: String;
    expiresIn: number;
    probability: string;
    encrypted?: number;
    expired?: Boolean;
};

export type Messages = Array<Message>;

export async function read (gameId: String): Promise<Messages | Memo> {
    try {
        const response = await fetch(kingdom + gameId + '/messages', {
            method: 'GET',
            headers
        });
        
        return response.json();
    } catch (e) {
        return {
            error: 'Unknown mistery caused all messages to be lost...'
        }
    }
}

export type Solution = {
    success?: Boolean;
    lives: number;
    gold: number;
    score: number;
    highScore: number;
    turn: number;
    message?: string;
};

export async function solve (gameId: String, adId: String): Promise<Solution | Memo> {
    try {
        const response = await fetch(kingdom + gameId + '/solve/' + adId, {
            method: 'POST',
            headers
        });
    
        return response.json();
    } catch (e) {
        return {
            error: 'Something came up and disrupted solution progress...'
        }
    }
}

export type Item = {
    id: string;
    name: string;
    cost: Number;
};

export type Items = Array<Item>;

export async function browse (gameId: String): Promise<Items | Memo> {
    try {
        const response = await fetch(kingdom + gameId + '/shop', {
            method: 'GET',
            headers
        });
    
        return response.json();
    } catch (e) {
        return {
            error: 'Villagers in panic, shop is closed!'
        }
    }
}

export type Receipt = {
    shoppingSuccess?: false;
    gold: number;
    lives: number;
    level: number;
    turn: number;
    itemId?: string;
};

export async function purchase (gameId: String, itemId: String): Promise<Receipt | Memo> {
    try {
        const response = await fetch(kingdom + gameId + '/shop/buy/' + itemId, {
            method: 'POST',
            headers
        });
    
        return response.json();
    } catch (e) {
        return {
            error: 'Something wrong in the purchase process...'
        }
    }
}

export type Matches = {
    [key: string]: String;
}

export type Match = keyof Matches;

export const match = (id: Match, matches: Matches): String => id in matches ? matches[id] : '';

export type Icons = {
    [key: string]: Matches;
}

export const icons: Icons = {
    status: {
        gold: '💰',
        level: '🐲',
        lives: '❤️',
        score: '🧮',
        turn: '🚶‍♂️',
        blackheart: '🖤',
        error: '🤷‍♂️',
        dead: '☠️',
        broken: '💔',
        success: '✅',
        defeat: '❌',
        loading: '🐉'
    },
    item: {
        hpot: '🧪',
        cs: '🦞',
        gas: '🛢',
        wax: '📀',
        tricks: '📕',
        wingpot: '⚗️',
        ch: '✂️',
        rf: '🚀',
        iron: '💿',
        mtrix: '📗',
        wingpotmax: '🍹',
    },
    reputation: {
        people: '👨‍👩‍👧‍👦',
        state: '🏰',
        underworld: '🔮',
    }
};

const show = (property: string, diff: number) => ({
    key: property,
    icon: match(property, icons.status) || null,
    value: diff > 0 ? '+' + diff : diff.toString(),
});


export const changes = (diff: Solution | Receipt) => Object.entries(diff).filter(([key, value]) => key !== 'highScore' && value !== 0).map(([key, value]) => show(key, value as number));

export const longings = {
    quests: 'Looking for quests...',
    solution: 'Solving the quest...',
    items: 'Calling for merchant...',
    receipt: 'Dealing with merchant...',
    reputation: 'Listening to the whispers...',
};

type Lookup = {
    [key: string]: string;
}

const input = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.split('');
const output = 'NOPQRSTUVWXYZABCDEFGHIJKLMnopqrstuvwxyzabcdefghijklm'.split('');
const lookup: Lookup = input.reduce((m, k, i) => Object.assign(m, {
  [k]: output[i]
}), {});

export const rot13 = (str: string) => str.split('').map((x: string) => lookup[x] || x).join('');

export const decrypt = (str: string, level: number = 0): string => level === 1 ? atob(str) : level === 2 ? rot13(str) : str;

export const encrypt = (str: string, level: number = 0): string => level === 1 ? btoa(str) : level === 2 ? rot13(str) : str;
