import React, { useState } from 'react';
import { Message, Game, icons, match, decrypt } from '../api';

export type GameMessage = {
    data: Message;
    game: Game;
    solve: Function;
}

export default function Message (props: GameMessage) {
    const [expires] = useState(props.game.turn + props.data.expiresIn);
    const expired = props.game.turn >= expires;
    const expire = expires - props.game.turn;

    const solve = (event: React.MouseEvent<HTMLElement>) => {
        event.preventDefault();

        if (expired || props.data.expired) {
            return;
        }

        props.solve(decrypt(props.data.adId, props.data.encrypted));
    }

    return <li className={'message' + (expired || props.data.expired ? ' -expired' : (expire === 3 ? ' -expiring-3' : expire === 2 ? ' -expiring-2' : expire === 1 ? ' -expiring-1' : ''))} data-encrypted={props.data.encrypted}>
            <h3 className="message-heading">{decrypt(props.data.message, props.data.encrypted)}</h3>

            <ul className="report">
                <li>{match('gold', icons.status)} <strong>+{props.data.reward}</strong></li>
        {
            expired || props.data.expired ?
                '' :
                <li className="message-expires">
                    {match('turn', icons.status)}
                    <span>{expire}</span>
                </li>
        }
            </ul>
            
            <div className="buttonbar -single">
                <button onClick={solve}>{decrypt(props.data.probability, props.data.encrypted)}</button>
            </div>
        </li>
}
