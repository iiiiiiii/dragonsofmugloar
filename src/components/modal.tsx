import React, { useEffect, useRef } from 'react';
import dialogPolyfill from 'dialog-polyfill';

export type GameModal = {
    open?: boolean | undefined;
    hide?: Function;
    children?: Element | string | JSX.Element | Array<JSX.Element>;
}

interface HTMLDialogElement extends HTMLElement {
    open: Boolean;
    returnValue: String;
    howModal: Function;
    showModal: Function;
    close: Function;
}

export default function Modal (props: GameModal) {
    const modal = useRef<HTMLDialogElement>(null);

    useEffect(() => {
        const self = modal.current as HTMLDialogElement;
        const show = () => self.showModal();
        const close = () => self.close();
        const action = props.open ? show : close;

        dialogPolyfill.registerDialog(self);

        action();
    }, []);

    return <dialog ref={modal}>
        {props.children}
    </dialog>
}
