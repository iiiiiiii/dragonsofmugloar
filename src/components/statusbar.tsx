import React from 'react';
import { Game, match, icons } from '../api';

export type GameStatusbar = {
    game: Game;
}

export default function Statusbar (props: GameStatusbar) {
    const lives = 'lives' in props.game && 'gameId' in props.game && props.game.gameId ? props.game.lives <= 0 ? match('dead', icons.status) : Array(props.game.lives > 5 ? 5 : props.game.lives).fill('').map(() => match('lives', icons.status)).join(' ') : match('blackheart', icons.status);

    return <ul className="statusbar">
        <li>{lives}{props.game.lives > 5 ? '+' + (props.game.lives - 5) : ''}</li>
        <li>{match('gold', icons.status)} <strong>{props.game.gold}</strong></li>
        <li>{match('level', icons.status)} <strong>{props.game.level}</strong></li>
        <li>{match('score', icons.status)} <strong>{props.game.score}</strong> {props.game.highScore ? '(' + props.game.highScore + ')' : ''}</li>
        <li>{match('turn', icons.status)} <strong>{props.game.turn}</strong></li>
    </ul>
}
