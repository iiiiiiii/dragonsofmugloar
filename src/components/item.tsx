import React from 'react';
import { Item, match, icons } from '../api';

export type GameItem = {
    data: Item;
    buy: Function;
    gold: Number;
}

export default function Item (props: GameItem) {
    const buy = (event: React.MouseEvent<HTMLElement>) => {
        event.preventDefault();

        props.buy(props.data.id)
    }
    const icon = match(props.data.id, icons.item) || null;

    return <li className="item">
            {icon && <p className="item-icon -oversize">{icon}</p>}
            <h3 className="item-name">{props.data.name}</h3>
            <div className="buttonbar -spread">
            <span className="item-cost">{match('gold',icons.status)} {props.data.cost}</span>
            <button onClick={buy} disabled={props.gold < props.data.cost}>Buy</button>
            </div>
        </li>
}
