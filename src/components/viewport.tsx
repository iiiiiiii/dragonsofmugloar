import React from 'react'

export type GameViewport = {
    children?: JSX.Element | Array<JSX.Element>;
}

export default function Viewport (props: GameViewport) {
    return <>
        <h1>Dragons of Mugloar</h1>
        {props.children}
    </>;
}