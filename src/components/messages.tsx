import React, { ReactNode } from 'react';

export type GameMessages = {
    children?: ReactNode | undefined;
}

export default function Messages (props: GameMessages) {
    return <ul className="messages">
        {props.children}
    </ul>;
}
