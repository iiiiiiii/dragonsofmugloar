import React, { ReactNode } from 'react';

export type GameItems = {
    children?: ReactNode | undefined;
}

export default function Items (props: GameItems) {
    return <ul className="items">
        {props.children}
    </ul>;
}
