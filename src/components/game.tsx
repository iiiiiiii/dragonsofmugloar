import React, { useState } from 'react';
import * as Dragon from '../api';
import { New, Main, Board, Shop } from '../screens';
import Statusbar from './statusbar';
import Viewport from './viewport';

export default function Game () {
    const [memo, setMemo] = useState<Dragon.Memo>({});
    const stored = localStorage.getItem('game') || '{}';
    const [game, setGame] = useState<Dragon.Game>(JSON.parse(stored));
    const [screen, setScreen] = useState(game.gameId ? 'main' : 'new');
    const [loading, setLoading] = useState<Boolean>(false);

    const start = () => {
        setLoading(true);

        Dragon.start().then(status => {
            setLoading(false);

            if ('error' in status || 'status' in status) {
                setMemo(status as Dragon.Memo);

                return;
            }

            const s = status as Dragon.Game;
            const highscored = s.highScore ? s : game.highScore ? { ...s, highScore: game.highScore } : s;

            setGame(highscored);
            setScreen('main');

            localStorage.setItem('game', JSON.stringify(highscored));
        });
    };

    const quit = () => {
        update({
            ...game,
            gameId: '',
            highScore: game.score > game.highScore ? game.score : game.highScore
        });
        setScreen('new');
        setMemo({});
    };

    const update = (changes: Dragon.Game) => {
        const highscored = {
            ...changes,
            highScore: changes.highScore ? changes.highScore : game.highScore
        }
        const merged = { ...game, ...highscored };

        setGame(merged);
        localStorage.setItem('game', JSON.stringify(merged));
    }

    const back = () => {
        setScreen('main');
    }

    const output = [<Statusbar key="statusbar" game={game} />];

    switch (screen) {
        case 'new':
            output.push(<New key={screen} loading={loading} memo={memo} setMemo={setMemo} start={start} />);

            break;

        case 'board':
            output.push(<Board key={screen} game={game} update={update} quit={quit} return={back} />);

            break;

        case 'shop':
            output.push(<Shop key={screen} game={game} update={update} quit={quit} return={back} />);

            break;

        case 'main':
        default:    
            output.push(<Main key={screen} game={game} navigate={setScreen} quit={quit} />);

            break;
    }

    return <Viewport>
        {output.map(element => element)}
    </Viewport>
}