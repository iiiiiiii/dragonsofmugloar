import React from 'react';
import ReactDOM from 'react-dom';
import Game from './components/game';
import Statusbar from './components/statusbar';
import Viewport from './components/viewport';
import './index.css';

ReactDOM.render(
  <React.StrictMode>
    <Game />
  </React.StrictMode>,
  document.querySelector('body')
);
